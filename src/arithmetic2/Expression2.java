package arithmetic2;

import java.util.Random;

public class Expression2 {
    String result;

    public Expression2() {
        result = "";
    }

    public String getResult(){
        return result;
    }

    //只有加减等级一
    public void level1() {
        {
            Random a = new Random();
            //是否有括号
            int or = a.nextInt(2);
            //无括号
            if (or == 0) {
                //几个数相加或相减
                int time = a.nextInt(2) + 2;
                Num2 b = new Num2();
                for (int index = 0; index <= time; index++) {
                    b.getNum();
                    int el = a.nextInt(2);
                    Elements2 c = new Elements2();
                    if (el == 0) {
                        c.add();
                        result += b.toString() + c;
                    } else {
                        c.sub();
                        result += b.toString() + c;
                    }
                }
                b.getNum();
                result += b.toString();
            }
            //有括号
            else {
                //先做出括号并且做出括号里的内容
                String kuohao = "";
                int ge = a.nextInt(2) + 2;
                Elements2 c = new Elements2();
                c.khq();
                kuohao += c;
                for (int i = 0; i <= ge; i++) {
                    Num2 b = new Num2();
                    b.getNum();
                    kuohao += b.toString();
                    int el = a.nextInt(2);
                    if (el == 0) {
                        c.add();
                        kuohao += c;
                    } else {
                        c.sub();
                        kuohao += c;
                    }
                }
                Num2 e = new Num2();
                e.getNum();
                c.khh();
                kuohao += e.toString() + c;
                int local = a.nextInt(2);
                //括号在中间，先把括号前的内容加上
                if (local == 0) {
                    int time = a.nextInt(3) + 1;
                    Num2 b = new Num2();
                    for (int index = 0; index <= time; index++) {
                        b.getNum();
                        int el = a.nextInt(2);
                        if (el == 0) {
                            c.add();
                            result += b.toString() + c;
                        } else {
                            c.sub();
                            result += b.toString() + c;
                        }
                    }
                    result += kuohao;/*加入括号*/
                    //再把括号后的内容加上
                    int times = a.nextInt(3) + 1;
                    Num2 d = new Num2();
                    for (int index = 0; index <= times; index++) {
                        d.getNum();
                        int el = a.nextInt(2);
                        if (el == 0) {
                            c.add();
                            result += c + d.toString();
                        } else {
                            c.sub();
                            result += c + d.toString();
                        }
                    }
                }
                //括号在后，把括号前的内容加上
                else {
                    int time = a.nextInt(3) + 1;
                    Num2 b = new Num2();
                    for (int index = 0; index <= time; index++) {
                        b.getNum();
                        int el = a.nextInt(2);
                        if (el == 0) {
                            c.add();
                            result += b.toString() + c;
                        } else {
                            c.sub();
                            result += b.toString() + c;
                        }
                    }
                    result += kuohao;/*再把括号加上*/
                }
            }
        }
        result += " = ";
    }
    //只有乘除等级二
    public void level2() {
        {
            Random a = new Random();
            //是否有括号
            int or = a.nextInt(2);
            //无括号
            if (or == 0) {
                //几个数相乘或相除
                int time = a.nextInt(2) + 2;
                Num2 b = new Num2();
                for (int index = 0; index <= time; index++) {
                    b.getNum();
                    int el = a.nextInt(2);
                    Elements2 c = new Elements2();
                    if (el == 0) {
                        c.mul();
                        result += b.toString() + c;
                    } else {
                        c.div();
                        result += b.toString() + c;
                    }
                }
                b.getNum();
                result += b.toString();
            }
            //有括号
            else {
                //做出括号
                String kuohao = "";
                int ge = a.nextInt(2) + 2;
                Elements2 c = new Elements2();
                c.khq();
                kuohao += c;
                for (int i = 0; i <= ge; i++) {
                    Num2 b = new Num2();
                    b.getNum();
                    kuohao += b.toString();
                    int el = a.nextInt(2);
                    if (el == 0) {
                        c.mul();
                        kuohao += c;
                    } else {
                        c.div();
                        kuohao += c;
                    }
                }
                Num2 e = new Num2();
                e.getNum();
                c.khh();
                kuohao += e.toString() + c;
                int local = a.nextInt(2);
                //括号在中，先把括号前的内容加上
                if (local == 0) {
                    int time = a.nextInt(3) + 1;
                    Num2 b = new Num2();
                    for (int index = 0; index <= time; index++) {
                        b.getNum();
                        int el = a.nextInt(2);
                        if (el == 0) {
                            c.mul();
                            result += b.toString() + c;
                        } else {
                            c.div();
                            result += b.toString() + c;
                        }
                    }
                    result += kuohao;/*加入括号*/
                    //把括号后的内容加上
                    int times = a.nextInt(3) + 1;
                    Num2 d = new Num2();
                    for (int index = 0; index <= times; index++) {
                        d.getNum();
                        int el = a.nextInt(2);
                        if (el == 0) {
                            c.mul();
                            result += c + d.toString();
                        } else {
                            c.div();
                            result += c + d.toString();
                        }
                    }
                }
                //括号在后，先把括号前的内容加上
                else {
                    int time = a.nextInt(3) + 1;
                    Num2 b = new Num2();
                    for (int index = 0; index <= time; index++) {
                        b.getNum();
                        int el = a.nextInt(2);
                        if (el == 0) {
                            c.mul();
                            result += b.toString() + c;
                        } else {
                            c.div();
                            result += b.toString() + c;
                        }
                    }
                    result += kuohao;/*把括号加上*/
                }
            }
        }
        result += " = ";
    }
    //加减乘除混合等级三
    public void level3() {
        {
            Random a = new Random();
            //是否有括号
            int or = a.nextInt(2);
            //无括号
            if (or == 0) {
                Random time = new Random();
                int times = time.nextInt(2) + 2;
                int count1 = 0;//判断是否有加减法
                int count2 = 0;//判断是否有乘除法
                Num2 num = new Num2();
                for (int index = 0; index <= times; index++) {
                    int c = time.nextInt(4);
                    Elements2 q = new Elements2();
                    //开始生成表达式
                    if (c == 0) {
                        num.getNum();
                        q.add();
                        result += num.toString() + q;
                        count1++;
                    }
                    if (c == 1) {
                        num.getNum();
                        q.sub();
                        result += num.toString() + q;
                        count1++;
                    }
                    if (c == 2) {
                        num.getNum();
                        q.mul();
                        result += num.toString() + q;
                        count2++;
                    }
                    if (c == 3) {
                        num.getNum();
                        q.div();
                        result += num.toString() + q;
                        count2++;
                    }
                }
                num.getNum();
                result += num.toString();
                //分别判断是否有无加减或乘除，并进行相应的随机添加加减或者乘除法，以保证达到混合运算的目的。
                Elements2 p = new Elements2();
                Num2 w = new Num2();
                w.getNum();
                if (count1 != 0 && count2 != 0)
                    result = result;
                else if (count1 == 0) {
                    Random m = new Random();
                    int n = m.nextInt(2);
                    //选择加还是减
                    if (n == 0) {
                        p.add();
                        result += p + w.toString();
                    } else {
                        p.sub();
                        result += p + w.toString();
                    }
                } else {
                    Random m = new Random();
                    int n = m.nextInt(2);
                    //选择乘还是除
                    if (n == 0) {
                        p.mul();
                        result += p + w.toString();
                    } else {
                        p.div();
                        result += p + w.toString();
                    }
                }
            }
            //有括号
            else {
                //做出括号
                String kuohao = "";
                int ge = a.nextInt(2) + 2;
                Elements2 c = new Elements2();
                c.khq();
                kuohao += c;
                Num2 kh = new Num2();
                kh.getNum();
                kuohao += kh.toString();
                for (int i = 0; i <= ge; i++) {
                    int el = a.nextInt(2);
                    if (el == 0) {
                        c.add();
                        kh.getNum();
                        kuohao += c + kh.toString();
                    } else {
                        c.sub();
                        kh.getNum();
                        kuohao += c + kh.toString();
                    }
                }
                c.khh();
                kuohao += c;
                int you = a.nextInt(3);
                //括号在前
                if (you == 0) {
                    result += kuohao;/*先加括号*/
                    Random time = new Random();
                    int times = time.nextInt(3) + 1;
                    int count2 = 0;//判断是否有乘除法
                    Num2 num = new Num2();
                    for (int index = 0; index <= times; index++) {
                        int we = time.nextInt(4);
                        Elements2 q = new Elements2();
                        //开始生成表达式
                        if (we == 0) {
                            num.getNum();
                            q.add();
                            result += q + num.toString();
                        }
                        if (we == 1) {
                            num.getNum();
                            q.sub();
                            result += q + num.toString();
                        }
                        if (we == 2) {
                            num.getNum();
                            q.mul();
                            result += q + num.toString();
                            count2++;
                        }
                        if (we == 3) {
                            num.getNum();
                            q.div();
                            result += q + num.toString();
                            count2++;
                        }
                    }
                    //分别判断是否有乘除，并进行相应的乘除法添加，以保证达到混合运算的目的。
                    Elements2 p = new Elements2();
                    Num2 w = new Num2();
                    w.getNum();
                    if (count2 == 0) {
                        Random m = new Random();
                        int n = m.nextInt(2);
                        if (n == 0) {
                            p.mul();
                            result += p + w.toString();
                        } else {
                            p.div();
                            result += p + w.toString();
                        }
                    } else
                        result = result;
                }
                //括号在后，先做出括号前的式子
                if (you == 1) {
                    Random time = new Random();
                    int times = time.nextInt(3) + 1;
                    int count2 = 0;//判断是否有乘除法
                    Num2 num = new Num2();
                    for (int index = 0; index <= times; index++) {
                        int we = time.nextInt(4);
                        Elements2 q = new Elements2();
                        //开始生成表达式
                        if (we == 0) {
                            num.getNum();
                            q.add();
                            result += num.toString() + q;
                        }
                        if (we == 1) {
                            num.getNum();
                            q.sub();
                            result += num.toString() + q;
                        }
                        if (we == 2) {
                            num.getNum();
                            q.mul();
                            result += num.toString() + q;
                            count2++;
                        }
                        if (we == 3) {
                            num.getNum();
                            q.div();
                            result += num.toString() + q;
                            count2++;
                        }
                    }
                    //分别判断是否有乘除，并进行相应的乘除法添加，以保证达到混合运算的目的。
                    Elements2 p = new Elements2();
                    Num2 w = new Num2();
                    w.getNum();
                    if (count2 != 0)
                        result = result;
                        //选择乘还是除
                    else {
                        Random ac = new Random();
                        int aw = a.nextInt(2);
                        if (aw == 0) {
                            p.mul();
                            result += w.toString() + p;
                        } else {
                            p.div();
                            result += w.toString() + p;
                        }
                    }
                    result += kuohao;/*插入括号*/
                }
                //括号在中，先做出括号前的式子
                if (you == 2) {
                    int ac = a.nextInt(4);
                    Num2 b = new Num2();
                    Elements2 cd = new Elements2();
                    int times = a.nextInt(3) + 1;
                    int count = 0;
                    for (int in = 0; in <= times; in++) {
                        if (ac == 0) {
                            b.getNum();
                            cd.add();
                            result += b.toString() + cd;
                        }
                        if (ac == 1) {
                            b.getNum();
                            cd.sub();
                            result += b.toString() + cd;
                        }
                        if (ac == 2) {
                            b.getNum();
                            cd.mul();
                            result += b.toString() + cd;
                            count++;
                        }
                        if (ac == 3) {
                            b.getNum();
                            cd.div();
                            result += b.toString() + cd;
                            count++;
                        }
                    }
                    result += kuohao;/*加入括号*/
                    //括号后的式子
                    int time = a.nextInt(3) + 1;
                    Elements2 ad = new Elements2();
                    for (int in = 0; in <= time; in++) {
                        if (ac == 0) {
                            ad.add();
                            b.getNum();
                            result += ad + b.toString();
                        }
                        if (ac == 1) {
                            ad.sub();
                            b.getNum();
                            result += ad + b.toString();
                        }
                        if (ac == 2) {
                            ad.mul();
                            b.getNum();
                            result += ad + b.toString();
                            count++;
                        }
                        if (ac == 3) {
                            ad.div();
                            b.getNum();
                            result += ad + b.toString();
                            count++;
                        }
                    }
                    //判断整个式子是否有乘除法，并添加相应的乘除法，以达到混合运算的目的
                    if (count == 0) {
                        int iw = a.nextInt(2);
                        if (iw == 0) {
                            cd.mul();
                            b.getNum();
                            result += cd + b.toString();
                        } else {
                            cd.div();
                            b.getNum();
                            result += cd + b.toString();
                        }
                    } else {
                        result = result;
                    }
                }
            }
        }
        result += " = ";
    }

    @Override
    public String toString() {
        return result ;
    }
}