package arithmetic2;

public class Elements2 {
    private String ele;

    public Elements2() {
        ele = "";
    }
    //加号
    public void add(){ ele = " + "; }
    //减号
    public void sub(){
        ele = " - ";
    }
    //乘号
    public void mul(){
        ele = " * ";
    }
    //除号
    public void div(){
        ele = " / ";
    }
    //前括号
    public void khq(){
        ele = "( ";
    }
    //后括号
    public void khh(){ ele = " )"; }
    //toString
    @Override
    public String toString() {
        return ele;
    }
}