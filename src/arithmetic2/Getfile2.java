package arithmetic2;

import java.io.*;
import java.util.List;
import java.text.NumberFormat;
import java.text.DecimalFormat;

public class Getfile2 {
    FileWriter fw, file;
    FileReader fr;
    NumberFormat fmt;
    DecimalFormat fm;
    int e;

    //往文件写入题目
    public void getWrite(String string) throws IOException {
        file = new FileWriter("四则运算.txt");
        file.write(string);
        file.flush();
        file.close();
    }

    //从文件读入题目
    public void getRead(int b) throws IOException {
        fr = new FileReader("四则运算.txt");
        fw = new FileWriter("四则运算.txt", true);
        BufferedReader buf = new BufferedReader(fr);
        String line, result;
        int c = 1;
        String res;
        Cal2 lt = new Cal2();
        //读取题目和用户输入的答案
        while ((line = buf.readLine()) != null && c <= b) {
            //输出读取的内容
            System.out.println(line);
            //找到等号的索引值
            int index = line.indexOf('=');
            //题目
            res = line.substring(0, index);
            //用户输入的值
            result = line.substring(index + 1);
            //计算题目的正确值
            List<String> list = lt.work(res);
            List<String> list2 = lt.InfixToPostfix(list);
            res = lt.doCal(list2) + "";
            fm = new DecimalFormat("0.####");/*把正确答案格式化，最多四位小数*/
            //把用户输入的值和正确结果全部转化为浮点值
            double result1 = Double.parseDouble(res);
            double result2 = Double.parseDouble(result);
            //进行判断，如果差值小于0.0001，就认为相等
            if (result2 - result1 < 0.0001 && result2 - result1 > -0.0001) {
                System.out.println("第" + c + "题,答案正确");
                e++;/*正确的题数*/
                fw.write("第" + c + "题,答案正确\r\n");
                fw.flush();
            } else {
                System.out.println("第" + c + "题结果错误，正确答案为" + fm.format(result1));
                fw.write("第" + c + "题结果错误，正确答案为" + fm.format(result1) + "\r\n");
                fw.flush();
            }
            c++; /*行数+1*/
        }
        fr.close();
        fw.write("正确率："+lt.T(e, b) + "");
        fw.flush();
        fw.close();
    }
}
