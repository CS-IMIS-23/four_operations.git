package arithmetic2;

import java.io.IOException;
import java.util.Scanner;

public class User3 {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        Getfile2 GF = new Getfile2();

        int a, b;
        String str,result="";

        System.out.println("请输入等级难度(1、2、3)：");
        a = scan.nextInt();

        System.out.println("请选择题目数：");
        b = scan.nextInt();

        //生成等级一的题目
        if (a == 1) {
            for (int index = 1; index <= b; index++) {
                Expression2 expression = new Expression2();
                expression.level1();
                System.out.println(expression);
                result +=expression+"\r\n";
            }
            GF.getWrite(result);
        }

        //生成等级二的题目
        else if (a == 2) {
            for (int index = 1; index <= b; index++) {
                Expression2 expression = new Expression2();
                expression.level2();
                System.out.println(expression);
                result +=expression+"\r\n";
            }
            GF.getWrite(result);
        }

        //生成等级三的题目
        else if (a == 3) {
            for (int index = 1; index <= b; index++) {
                Expression2 expression = new Expression2();
                expression.level3();
                System.out.println(expression);
                result +=expression+"\r\n";
            }
            GF.getWrite(result);
        }
        System.out.println("题目生成完毕，请在文件中作答（四位小数）。");
        System.out.println("是否答题完毕？");
        str = scan.next();
        if (str.equalsIgnoreCase("y"))
        GF.getRead(b);
    }
}
