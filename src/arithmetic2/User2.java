package arithmetic2;

import java.util.List;
import java.util.Scanner;

public class User2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double result;
        Cal2 lt = new Cal2();

        int a, b,c=0;

        System.out.println("请输入等级难度(1、2、3)：");
        a = scan.nextInt();

        System.out.println("请选择题目数：");
        b = scan.nextInt();

        //生成等级一的题目
        if (a == 1) {
            for (int index = 1; index <= b; index++) {
                Expression2 expression = new Expression2();
                expression.level1();
                System.out.println("题目：" + expression);

                System.out.println("请输入你的答案：");
                result = scan.nextDouble();

                List<String> list = lt.work(expression.getResult());
                List<String> list2 = lt.InfixToPostfix(list);


                if (lt.doCal(list2) - result < 0.0001 && lt.doCal(list2) - result > -0.0001) {
                    System.out.println("结果正确。");
                    c++;
                }
                else
                    System.out.println("结果错误，答案为" + lt.doCal(list2));
            }
        }
        //生成等级二的题目
        else if (a == 2) {
            for (int index = 1; index <= b; index++) {
                Expression2 expression = new Expression2();
                expression.level2();
                System.out.println("题目：" + expression);

                System.out.println("请输入你的答案：");
                result = scan.nextDouble();

                List<String> list = lt.work(expression.getResult());
                List<String> list2 = lt.InfixToPostfix(list);


                if (lt.doCal(list2) - result < 0.0001 && lt.doCal(list2) - result > -0.0001) {
                    System.out.println("结果正确。");
                    c++;
                }
                else
                    System.out.println("结果错误，答案为" + lt.doCal(list2));
            }
        }
        //生成等级三的题目
        else if (a == 3) {
            for (int index = 1; index <= b; index++) {
                Expression2 expression = new Expression2();
                expression.level3();
                System.out.println("题目：" + expression);

                System.out.println("请输入你的答案：");
                result = scan.nextDouble();

                List<String> list = lt.work(expression.getResult());
                List<String> list2 = lt.InfixToPostfix(list);


                if (lt.doCal(list2) - result < 0.0001 && lt.doCal(list2) - result > -0.0001) {
                    System.out.println("结果正确。");
                    c++;
                }
                else
                    System.out.println("结果错误，答案为" + lt.doCal(list2));
            }
        }
        lt.T(c,b);
    }
}
