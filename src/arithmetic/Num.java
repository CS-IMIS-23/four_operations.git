package arithmetic;

import java.util.Random;

public class Num {
    private int numerator, denominator;
    private String number;
    //构造函数
    public Num() {
        numerator = 0;
        denominator = 0;
        number = "";
    }
    //得到数
    public void getNum() {
        Random num = new Random();
        numerator = num.nextInt(5);//分子
        denominator = num.nextInt(5)+1;//分母
        if (denominator == 1 && numerator != 0)//分母为1,结果为分子。
            number = numerator + "";
        else if (numerator == 0)//分子为0，结果为分母。
            number = denominator + "";
        else if (denominator == numerator )
            number = 1 + "";
        else {
            reduce();//约分
            if (numerator < denominator)//约分后分子小于分母
                number = numerator + "/" + denominator;
            else if (numerator > denominator)//约分后分子大于分母，交换分子分母
                number = +denominator + "/" + numerator;
        }
    }
    //约分的方法
    private int gcd(int num1,int num2)
    {
        while (num1 !=num2) {
            if (num1 > num2)
                num1 = num1 - num2;
            else
                num2 = num2 - num1;
        }

        return num1;
    }
    private void reduce()
    {
        if (numerator !=0)
        {
            int common = gcd(Math.abs(numerator),denominator);

            numerator = numerator/common;
            denominator = denominator/common;
        }
    }
    //toString
    public String toString(){
        return number;
    }
}
