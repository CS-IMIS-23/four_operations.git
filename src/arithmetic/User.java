package arithmetic;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class User {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        String arg;
        System.out.println("是否开始四则运算？");
        arg = scan.nextLine();

        while (arg.equalsIgnoreCase("y")) {
            int a, b;
            System.out.println("请输入等级难度(1、2、3)：");
            a = scan.nextInt();
            System.out.println("请选择题目数：");
            b = scan.nextInt();
            if (a == 1) {
                for (int index = 1; index <= b; index++) {
                    Expression expression = new Expression();
                    expression.level1();
                    System.out.println(expression);

                    Cal cal = new Cal();
                    List<String> list = cal.work(expression + "");
                    list = cal.InfixToPostfix(list);
                    double result = cal.doCal(list);
                    cal.torf(list);
                    int cd = cal.getAccount();
                    System.out.println(cd);

                }
            } else if (a == 2) {
                for (int index = 1; index <= b; index++) {
                    Expression expression = new Expression();
                    expression.level2();
                    System.out.println(expression);

                }
            } else if (a == 3) {
                for (int index = 1; index <= b; index++) {
                    Expression expression = new Expression();
                    expression.level3();
                    System.out.println(expression);
                }
            } else {
                System.out.println("难度等级输入错误");
            }
            System.out.println("继续(y)，结束并且判断正确率(n)：");
            arg = scan.next();

        }
    }
}
