package arithmetic;

import java.io.*;
import java.util.List;
import java.util.Scanner;
import java.text.NumberFormat;
import java.text.DecimalFormat;
public class GetFile {
    FileWriter file,fw;
    FileReader fr;
    Scanner scan = new Scanner(System.in);
    int a, b,e;
    NumberFormat fmt;
    DecimalFormat fm;
    //写入(题目)
    public void getWrite() throws IOException {
        file = new FileWriter("E:\\four_operations\\四则运算.txt");
        BufferedWriter bw = new BufferedWriter(file);

        System.out.println("请输入等级难度(1、2、3)：");
        a = scan.nextInt();

        System.out.println("请选择题目数：");
        b = scan.nextInt();
        //生成等级一的题目
        if (a == 1) {
            for (int index = 1; index <= b; index++) {
                Expression expression = new Expression();
                expression.level1();
                file.write(expression + "");
                bw.newLine();
                file.flush();
                bw.flush();
            }
        }
        //生成等级二的题目
        else if (a == 2) {
            for (int index = 1; index <= b; index++) {
                Expression expression = new Expression();
                expression.level2();
                file.write(expression + "");
                bw.newLine();
                file.flush();
                bw.flush();
            }
        }
        //生成等级三的题目
        else if (a == 3) {
            for (int index = 1; index <= b; index++) {
                Expression expression = new Expression();
                expression.level3();
                file.write(expression + "");
                bw.newLine();
                file.flush();
                bw.flush();
            }
        }
        System.out.println("试卷已生成，请作答。");
        file.close();
        bw.close();
    }

    //读入（题目进行计算并且判断正误）并且把计算结果写入文件
    public void getRead()throws IOException{
            fr = new FileReader("E:\\four_operations\\四则运算.txt");
            fw = new FileWriter("E:\\four_operations\\四则运算.txt",true);
            BufferedReader buf = new BufferedReader(fr);
            String line,result;
            int c = 1;
            String res;
            //读取题目和用户输入的答案
            while ((line = buf.readLine())!= null && c <= b) {
                    //输出读取的内容
                    System.out.println(line);
                    //找到等号的索引值
                    int index = line.indexOf('=');
                    //题目
                    res = line.substring(0,index);
                    //用户输入的值
                    result = line.substring(index+1);
                    //计算题目的正确值
                    doCal lt = new doCal();
                    List<String> list = lt.work(res);
                    List<String> list2 = lt.InfixToPostfix(list);
                    res = lt.doCal(list2) + "";
                    fm = new DecimalFormat("0.####");/*把正确答案格式化，最多四位小数*/
                    //把用户输入的值和正确结果全部转化为浮点值
                    double result1 = Double.parseDouble(res);
                    double result2 = Double.parseDouble(result);
                    //进行判断，如果差值小于0.0001，就认为相等
                    if (result2 - result1 < 0.0001 && result2 - result1 > -0.0001) {
                        System.out.println("第"+ c  +"题,答案正确");
                        e++;/*正确的题数*/
                        fw.write("第" + c + "题,答案正确\r\n");
                        fw.flush();
                    }
                    else {
                        System.out.println("第"+ c  +"题结果错误，正确答案为" + fm.format(result1) );
                        fw.write("第" + c + "题结果错误，正确答案为" + fm.format(result1) + "\r\n");
                        fw.flush();
                    }
                    c++; /*行数+1*/
            }
            fr.close();
            fw.close();
    }
    //正确率计算
    public void ReadTorF() throws IOException {
        fw = new FileWriter("E:\\four_operations\\四则运算.txt",true);
        double t = (double)e/b;/*正确的题数÷总题数*/
        fmt = NumberFormat.getPercentInstance();
        System.out.println("正确率："+fmt.format(t));/*格式化为百分数*/
        fw.write("正确率："+fmt.format(t));
        fw.flush();
        fw.close();
        }
}